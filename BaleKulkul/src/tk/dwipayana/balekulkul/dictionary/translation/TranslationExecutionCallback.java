package tk.dwipayana.balekulkul.dictionary.translation;


public interface TranslationExecutionCallback {
	void deletePreviousTranslationResult();
	void newTranslationResult(TranslationResult resultOfTranslation);
}
