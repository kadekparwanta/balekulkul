package tk.dwipayana.balekulkul.dictionary.general;

public class CouldNotOpenFileException extends DictionaryException {
	
	public CouldNotOpenFileException() {
		super("File could not be opened");
	}

	public CouldNotOpenFileException(String message) {
		super(message);
	}

	public CouldNotOpenFileException(Throwable t) {
		super(t);
	}


}
