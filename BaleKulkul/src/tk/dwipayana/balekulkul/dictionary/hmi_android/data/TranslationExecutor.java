package tk.dwipayana.balekulkul.dictionary.hmi_android.data;

import tk.dwipayana.balekulkul.dictionary.general.DictionaryException;
import tk.dwipayana.balekulkul.dictionary.translation.TranslationExecutionCallback;
import tk.dwipayana.balekulkul.dictionary.translation.TranslationParameters;

public interface TranslationExecutor {

	public void setTranslationExecutionCallback(
			TranslationExecutionCallback translationResultHMIObjParam);

	public void executeTranslation(TranslationParameters translationParametersObj)
			throws DictionaryException;

	public void cancelLastTranslation();

}
