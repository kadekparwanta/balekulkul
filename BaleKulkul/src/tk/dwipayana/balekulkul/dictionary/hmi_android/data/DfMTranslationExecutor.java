package tk.dwipayana.balekulkul.dictionary.hmi_android.data;

import tk.dwipayana.balekulkul.dictionary.general.DictionaryException;
import tk.dwipayana.balekulkul.dictionary.translation.TranslationExecution;
import tk.dwipayana.balekulkul.dictionary.translation.TranslationExecutionCallback;
import tk.dwipayana.balekulkul.dictionary.translation.TranslationParameters;

public class DfMTranslationExecutor implements TranslationExecutor {

	@Override
	public void setTranslationExecutionCallback(
			TranslationExecutionCallback translationResultHMIObjParam) {
		TranslationExecution.setTranslationExecutionCallback(translationResultHMIObjParam);
	}

	@Override
	public void executeTranslation(TranslationParameters translationParametersObj)
			throws DictionaryException {
		TranslationExecution.executeTranslation(translationParametersObj);
	}

	@Override
	public void cancelLastTranslation() {
		TranslationExecution.cancelLastTranslation();
	}

}
